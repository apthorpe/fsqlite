# fsqlite

Updated CMake-enabled port of Arjen Markus' Fortran interface to SQLite3

## Status

This module is under active development and many sections still require documentation and testing. It builds, runs, and packages on Windows, Linux, and MacOS. PDF documentation can be generated on Windows and Linux; MacOS has difficulty finding the TeX Gyre Pagella font, likely due to spaces in the typeface name.

## Design Goals

Build a maintainable CMake configuration for the Fortran interface to SQLite3 in Arjen Markus' FLIBS package - see http://flibs.sourceforge.net/

CMake is used as the principal build automation tool and the library should build, test, and package on recent Windows, Linux, and MacOS systems.

Doxygen is used as the principal documentation tool since it can produce formal documentation in PDF format via LaTeX.

## Example Usage

See test applications `myfprog.f90` and `csvtable.f90`. This project's `CMakeLists.txt` file illustrates how to build `fsqlite` as a static library and how to link it to a Fortran application.
## Availability

The project is currently hosted at https://gitlab.com/apthorpe/fsqlite

## Contibutor Guidance

Potential contributors are expected to treat each other with respect and dignity; see `CODE_OF_CONDUCT.md` for details.

With that out of the way, I would appreciate issue reports, test results, and suggestions for improvement at https://gitlab.com/apthorpe/fsqlite/-/issues

## License

Copyright (c) 2021 Bob Apthorpe, released under the BSD 3-Clause License.