/**
 * @file csqlite.c
 *
 * @brief C wrappers callable from Fortran for the SQLite library
 *
 * @copyright See LICENSE; BSD-3 Clause
 *
 */

/* csqlite.c --
      C wrappers callable from Fortran for the SQLite library

      $Id: csqlite.c,v 1.4 2008/05/04 13:23:56 arjenmarkus Exp $
*/
#define FTNCALL

#include <string.h>
#include "sqlite3.h"

/** @brief Result table for sqlite3_get_table
 */
static char **result;

/** @brief Callback function
 *  @note Does nothing; returns zero for all input
 */
static int callback(
   /** Unused */
   void *NotUsed,
   /** Argument count */
   int argc,
   /** Argument values */
   char **argv,
   /** Unused */
   char **azColName){
  /*
  int i;
  for(i=0; i<argc; i++){
    printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
  }
  printf("\n");
  */
  return 0;
}

/** @brief Opens SQLite database
 *
 *  @return Integer status code; 0 is success, nonzero is failure
 */
int FTNCALL sqlite3_open_c(
       /** Database file name, input */
       char     *fname,
       /** Database handle, input, output */
       sqlite3 **db,
       /** String length of database file name, input. Not used. */
       int       len_fname
      )
{
   int rc ;

   rc = sqlite3_open(fname, db);

   if ( rc != 0 )
   {
      sqlite3_close(*db);
   }
   return rc ;
}

/** @brief Closes SQLite database
 *
 *  @return Integer status code; 0 is success, nonzero is failure
 */
int FTNCALL sqlite3_close_c(
       /** Database handle, input, output */
       sqlite3 **db
      )
{
   int rc ;

   rc = sqlite3_close(*db);
   return rc ;
}

/** @brief Executes SQLite command
 *
 *  @return Integer status code; 0 is success, nonzero is failure
 */
int FTNCALL sqlite3_do_c(
       /** Database handle, input */
       sqlite3 **db,
       /** Database command, input */
       char *command,
       /** Database status message, output */
       char *errmsg,
       /** Database command length, input */
       int   len_command,
       /** Database status message length, output */
       int   len_errmsg
      )
{
   int   rc  ;
   char *msg ;

   rc = sqlite3_exec(*db, command, callback, 0, &msg ) ;
   if ( msg != NULL )
   {
      strncpy( errmsg, msg, len_errmsg ) ;
   }
   return rc ;
}

/** @brief Finalize prepared statement
 */
void FTNCALL sqlite3_finalize_c(
       /** Prepared statement */
       sqlite3_stmt **stmt
      )
{
   int   rc  ;

   rc = sqlite3_finalize( *stmt ) ;

   return ;
}

/** @brief Reset the prepared SQL statement so that it can be used again
 */
void FTNCALL sqlite3_reset_c(
       /** Prepared statement */
       sqlite3_stmt **stmt
      )
{
   int   rc  ;

   rc = sqlite3_reset( *stmt ) ;

   return ;
}

/** @brief Run the prepared SQL statement
 */
void FTNCALL sqlite3_step_c(
       /** Handle to the prepared statement. Input/output */
       sqlite3_stmt **stmt,

       /** Return code, indicating if the command is complete or
        *  not (SQLITE_DONE, SQLITE_MISUSE or SQLITE_ERROR). Output.
        */
       int           *completion
      )
{
   *completion = sqlite3_step( *stmt ) ;

   return ;
}

/** @brief Get the current database error message
 */
void FTNCALL sqlite3_errmsg_c(
       /** Handle to the database. Input. */
       sqlite3 **db,
       /** Error message. Output. */
       char *errmsg,
       /** Length of error message. Output. */
       int   len_errmsg
      )
{
   char *pstr ;

   pstr = sqlite3_errmsg( *db ) ;
   strncpy( errmsg, pstr, len_errmsg ) ;

   return ;
}

/** @brief Finalize prepared statement
 */
int FTNCALL sqlite3_prepare_c(
       /** Database handle */
       sqlite3      **db,
       /** Database command */
       char          *command,
       /** Database statement handle */
       sqlite3_stmt **stmt,
       /** Length of database command */
       int            len_command
      )
{
   int   rc   ;
   char *pstr ;

   rc = sqlite3_prepare( *db, command, (-1), stmt, &pstr ) ;

   return rc ;
}

/** @brief Count columns
 */
void FTNCALL sqlite3_column_count_c(
       /** Database statement handle */
       sqlite3_stmt **stmt,
       /** Number of columns */
       int           *count
      )
{
   *count = sqlite3_column_count( *stmt ) ;
   return ;
}

/** @brief Get column name and type
 */
void FTNCALL sqlite3_column_name_type_c(
       /** Database statement handle */
       sqlite3_stmt **stmt,
       /** Column index */
       int  *colidx,
       /** Column name */
       char *name,
       /** Data type name */
       char *type,
       /** Column name length */
       int   len_name,
       /** Data type name length */
       int   len_type
      )
{
   int   rc   ;
   char *pstr ;

   pstr = sqlite3_column_name(*stmt, *colidx ) ;
   strncpy( name, pstr, len_name ) ;
   name[len_name-1] = '\0' ;
   pstr = sqlite3_column_decltype(*stmt, *colidx ) ;
   strncpy( type, pstr, len_type ) ;
   type[len_type-1] = '\0' ;
   return ;
}

/** @brief Bind integer variable to column
 *  @returns Status code
 */
int FTNCALL sqlite3_bind_int_c(
       /** Database statement handle */
       sqlite3_stmt **stmt,
       /** Column index */
       int           *colidx,
       /** Column integer value */
       int           *value
      )
{
   int   rc   ;

   rc = sqlite3_bind_int(*stmt, *colidx, *value ) ;
   return rc ;
}

/** @brief Bind double variable to column
 *  @returns Status code
 */
int FTNCALL sqlite3_bind_double_c(
       /** Database statement handle */
       sqlite3_stmt **stmt,
       /** Column index */
       int           *colidx,
       /** Column double precision real value */
       double        *value
      )
{
   int   rc   ;

   rc = sqlite3_bind_double(*stmt, *colidx, *value ) ;
   return rc ;
}

/** @brief Bind null to column
 *  @returns Status code
 */
int FTNCALL sqlite3_bind_null_c(
       /** Database statement handle */
       sqlite3_stmt **stmt,
       /** Column index */
       int           *colidx
      )
{
   int   rc   ;

   rc = sqlite3_bind_null(*stmt, *colidx ) ;
   return rc ;
}

/** @brief Bind text variable to column
 *  @returns Status code
 */
int FTNCALL sqlite3_bind_text_c(
       /** Database statement handle */
       sqlite3_stmt **stmt,
       /** Column index */
       int           *colidx,
       /** Column text value */
       char          *text,
       /** Column text value length */
       int            len_text
      )
{
   int   rc   ;

   rc = sqlite3_bind_text(*stmt, *colidx, text, len_text,
           SQLITE_TRANSIENT ) ;
   return rc ;
}

/** @brief Retrive integer value from a column in query results
 *  @returns Status code
 */
int FTNCALL sqlite3_column_int_c(
       /** Database statement handle */
       sqlite3_stmt **stmt,
       /** Column index */
       int           *colidx,
       /** Column integer value */
       int           *value
      )
{
   *value = sqlite3_column_int(*stmt, *colidx ) ;
   return 0 ;
}

/** @brief Retrive double precision real value from a column in query
 *  results
 *
 *  @returns Status code
 */
int FTNCALL sqlite3_column_double_c(
       /** Database statement handle */
       sqlite3_stmt **stmt,
       /** Column index */
       int           *colidx,
       /** Column double precision real value */
       double        *value
      )
{
   *value = sqlite3_column_double(*stmt, *colidx ) ;
   return 0 ;
}

/** @brief Retrive text value from a column in query results
 *  @returns Status code
 */
int FTNCALL sqlite3_column_text_c(
       /** Database statement handle */
       sqlite3_stmt **stmt,
       /** Column index */
       int           *colidx,
       /** Column text value */
       char          *text,
       /** Column text value length */
       int            len_text
      )
{
   char *pstr ;

   pstr = sqlite3_column_text(*stmt, *colidx ) ;
   strncpy( text, pstr, len_text ) ;
   return 0 ;
}

/** @brief TBD
 *  @returns Status code
 */
int FTNCALL sqlite3_get_table_1_c(
       /** Database handle */
       sqlite3 **db,
       /** Database command string */
       char *command,
       /** Number of columns */
       int  *ncol,
       /** Number of rows */
       int  *nrow,
       /** Database error message string */
       char *errmsg,
       /** Database command string length */
       int   len_command,
       /** Database error message string length */
       int   len_errmsg
      )
{
   int   rc  ;
   char *msg ;

   rc = sqlite3_get_table(*db, command, &result, nrow, ncol, &msg ) ;
   if ( msg != NULL )
   {
      strncpy( errmsg, msg, len_errmsg ) ;
   }

   return rc ;
}

/** @brief TBD
 */
void FTNCALL sqlite3_get_table_2_c(
       /** Number of columns */
       int  *ncol,
       /** Number of rows */
       int  *nrow,
       /** Result table string */
       char *result_table,
       /** Result table string length */
       int   len_result
      )
{
   int   i   ;
   int   j   ;
   int   k   ;
   int   n   ;

   /* Note: one extra row! */
   for ( j = 0 ; j <= (*nrow) ; j ++ )
   {
      for ( i = 0 ; i < (*ncol) ; i ++ )
      {
         k = i + j*(*ncol) ;

         strncpy( &result_table[k*len_result], result[k], len_result ) ;

         for ( n = strlen(result[k]) ; n < len_result ; n ++ )
         {
            result_table[k*len_result+n] = ' ' ;
         }
      }
   }

   sqlite3_free_table( result ) ;
   result = NULL;

   return ;
}
