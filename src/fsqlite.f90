!> @file fsqlite.f90
!! @brief Module for interacting with SQLite3

!    $Id: fsqlite.f90,v 1.5 2011/01/19 11:36:46 arjenmarkus Exp $

!> @brief Module defining SQLite3 data and element types
!!
!! @author Arjen Markus
!!
!! This module and the associated C code is
!! inspired by work by Al Danial (http://danial.org).
!!
!! Further information about this interface can be found in
!! @cite ArjenMarkus2012a
module sqlite_types
   implicit none

   !> Double precision kind
   integer, parameter         :: dp = kind(1.0d00)

   ! The constants associated with data types in SQLite are:
   ! #define SQLITE_INTEGER  1
   ! #define SQLITE_FLOAT    2
   ! #define SQLITE_TEXT     3
   ! #define SQLITE_BLOB     4
   ! #define SQLITE_NULL     5
   ! Note that SQLite stores all reals as REAL64 (double precision)

   ! These datatypes exist in SQLite 3 and supported by fsqlite.
   ! Note that these constants refer to Fortran types, not SQLite types
   ! and their values do not align with the those defined in sqlite3.h

   !> Integer database type
   integer, parameter         :: SQLITE_INT    = 1

   !> Single(?) precision real database type
   integer, parameter         :: SQLITE_REAL   = 2

   !> Double precision real database type
   integer, parameter         :: SQLITE_DOUBLE = 3

   !> Character database type
   integer, parameter         :: SQLITE_CHAR   = 4

   ! These exist in SQLite 3 but are not currently supported by fsqlite:

   ! !> Binary large object (BLOB) database type
   ! integer, parameter         :: SQLITE_BLOB   = 5

   ! !> Null database type
   ! integer, parameter         :: SQLITE_NULL   = 6

   ! The following is the list of all basic SQLite result codes as of
   ! version 3.35.3 as parsed from sqlite.h:

   !> Basic SQLite result code: Successful result
   integer, parameter :: SQLITE_OK =           0

   !> Basic SQLite result code: Generic error
   integer, parameter :: SQLITE_ERROR =        1

   !> Basic SQLite result code: Internal logic error in SQLite
   integer, parameter :: SQLITE_INTERNAL =     2

   !> Basic SQLite result code: Access permission denied
   integer, parameter :: SQLITE_PERM =         3

   !> Basic SQLite result code: Callback routine requested an abort
   integer, parameter :: SQLITE_ABORT =        4

   !> Basic SQLite result code: The database file is locked
   integer, parameter :: SQLITE_BUSY =         5

   !> Basic SQLite result code: A table in the database is locked
   integer, parameter :: SQLITE_LOCKED =       6

   !> Basic SQLite result code: A malloc() failed
   integer, parameter :: SQLITE_NOMEM =        7

   !> Basic SQLite result code: Attempt to write a readonly database
   integer, parameter :: SQLITE_READONLY =     8

   !> Basic SQLite result code: Operation terminated by sqlite3_interrupt()
   integer, parameter :: SQLITE_INTERRUPT =    9

   !> Basic SQLite result code: Some kind of disk I/O error occurred
   integer, parameter :: SQLITE_IOERR =       10

   !> Basic SQLite result code: The database disk image is malformed
   integer, parameter :: SQLITE_CORRUPT =     11

   !> Basic SQLite result code: Unknown opcode in sqlite3_file_control()
   integer, parameter :: SQLITE_NOTFOUND =    12

   !> Basic SQLite result code: Insertion failed because database is full
   integer, parameter :: SQLITE_FULL =        13

   !> Basic SQLite result code: Unable to open the database file
   integer, parameter :: SQLITE_CANTOPEN =    14

   !> Basic SQLite result code: Database lock protocol error
   integer, parameter :: SQLITE_PROTOCOL =    15

   !> Basic SQLite result code: Internal use only
   integer, parameter :: SQLITE_EMPTY =       16

   !> Basic SQLite result code: The database schema changed
   integer, parameter :: SQLITE_SCHEMA =      17

   !> Basic SQLite result code: String or BLOB exceeds size limit
   integer, parameter :: SQLITE_TOOBIG =      18

   !> Basic SQLite result code: Abort due to constraint violation
   integer, parameter :: SQLITE_CONSTRAINT =  19

   !> Basic SQLite result code: Data type mismatch
   integer, parameter :: SQLITE_MISMATCH =    20

   !> Basic SQLite result code: Library used incorrectly
   integer, parameter :: SQLITE_MISUSE =      21

   !> Basic SQLite result code: Uses OS features not supported on host
   integer, parameter :: SQLITE_NOLFS =       22

   !> Basic SQLite result code: Authorization denied
   integer, parameter :: SQLITE_AUTH =        23

   !> Basic SQLite result code: Not used
   integer, parameter :: SQLITE_FORMAT =      24

   !> Basic SQLite result code: 2nd parameter to sqlite3_bind out of range
   integer, parameter :: SQLITE_RANGE =       25

   !> Basic SQLite result code: File opened that is not a database file
   integer, parameter :: SQLITE_NOTADB =      26

   !> Basic SQLite result code: Notifications from sqlite3_log()
   integer, parameter :: SQLITE_NOTICE =      27

   !> Basic SQLite result code: Warnings from sqlite3_log()
   integer, parameter :: SQLITE_WARNING =     28

   !> Basic SQLite result code: sqlite3_step() has another row ready
   integer, parameter :: SQLITE_ROW =         100

   !> Basic SQLite result code: sqlite3_step() has finished executing
   integer, parameter :: SQLITE_DONE =        101

   !> Statement data structure
   type SQLITE_STATEMENT
      !> Statement handle
      integer, dimension(2)   :: stmt_handle
   end type SQLITE_STATEMENT

   !> Database data structure
   type SQLITE_DATABASE
      !> Database handle
      integer, dimension(2)   :: db_handle
      !> Error code
      integer                 :: error
      !> Error message
      character(len=80)       :: errmsg
   end type SQLITE_DATABASE

   !> Database table column data structure
   type SQLITE_COLUMN
      !> Column name
      character(len=40)       :: name     = ' '
      !> Field type (name)
      character(len=40)       :: type     = ' '
      !> Funtion name
      character(len=40)       :: function = ' '
      !> Field type (integer)
      integer                 :: type_set
      !> Integer value of field
      integer                 :: int_value
      !> Double precision real value of field
      real(kind=dp)           :: double_value
      !> Character value of field
      character(len=80)       :: char_value
   end type SQLITE_COLUMN
end module sqlite_types

!> @brief Module for interacting with SQLite3
!! @author Arjen Markus
!!
!!    This module and the associated C code is
!!    inspired by work by Al Danial (http://danial.org).
!!    The purpose is to provide a high-level means
!!    for Fortran programmers to use the SQLite
!!    library by Richard Hipp (http://www.sqlite.org)
!!
!!    Detailed documentation can be found in
!!    the file fsqlite.html.
!!
!! @todo Arbitrary length character strings in set_column and get_column
!! @todo Think about finalize and reset: error code?
!! @todo Support BLOBs
!! @todo Support DATE
!! @todo Support NULL
!! @todo More elegant selection of functions of columns
!!
!! @note The handles to the database or prepared statements
!!    are stored in an array of two integers to take
!!    care of 64-bits platforms.
!! @note With the appropriate compilation options (if needed)
!!    the code should be thread-safe, as no data are shared.
module sqlite
   use sqlite_types
   implicit none

   private :: stringtof
   private :: stringtoc
   private :: typename
   private :: column_func

   !
   ! Convenient interfaces
   !
   !> Generic set column interface. Supports int, real, double, and char.
   interface sqlite3_set_column
      module procedure sqlite3_set_column_int
      module procedure sqlite3_set_column_real
      module procedure sqlite3_set_column_double
      module procedure sqlite3_set_column_char
   end interface

   !> Generic get column interface. Supports int, real, double, and char.
   interface sqlite3_get_column
      module procedure sqlite3_get_column_int
      module procedure sqlite3_get_column_real
      module procedure sqlite3_get_column_double
      module procedure sqlite3_get_column_char
   end interface

contains

! typename --
!    Construct the type and attributes of a column
!    in a new table
! Arguments:
!    column        Column information
!    primary       Name of the primary key

!> Construct the type and attributes of a column in a new table
character(len=40) function typename( column, primary )
   implicit none
   !> Column information
   type(SQLITE_COLUMN), intent(in) :: column
   !> Name of the primary key
   character(len=*), intent(in)    :: primary

   continue

   if ( column%name .ne. primary ) then
      typename = column%type
   else
      !write( typename, '(2a)' ) trim(column%type), ' primary key'
      typename = trim(column%type) // ' primary key'
   endif

   return
end function typename


! column_func --
!    Construct the name and function of a column
!    in a new table
! Arguments:
!    column        Column information
!

!> Construct the name and function of a column in a new table
character(len=80) function column_func( column )
   implicit none
   !> Column information
   type(SQLITE_COLUMN), intent(in) :: column
   continue

   if ( column%function .ne. ' ' ) then
      column_func = trim(column%function) // '(' // trim(column%name) // ')'
   else
      column_func = column%name
   endif

   return
end function column_func


! stringtof --
!    Convert a C string to Fortran
! Arguments:
!    string        String to be converted
!
!> Convert a C string to Fortran
subroutine stringtof( string )
   implicit none
   !> String to be converted
   character(len=*), intent(inout) :: string

   integer          :: last
   continue

   last = index( string, char(0) )
   if ( last .gt. 0 ) then
      string(last:) = ' '
   endif

   return
end subroutine stringtof


! stringtoc --
!    Convert a Fortran string to C
! Arguments:
!    string        String to be converted
! Note:
!    It is assumed that the last character
!    is a space. As this is a private
!    routine, this should have been taken
!    care of in the caller.
!
!> Convert a Fortran string to C
!!
!! @note It is assumed that the last character is a space. As this is
!! a private routine, this should have been taken care of in the caller.
subroutine stringtoc( string )
   implicit none
   !> String to be converted
   character(len=*), intent(inout) :: string

   integer          :: last
   continue

   last = 1 + len_trim(string)
   string(last:last) = char(0)

   return
end subroutine stringtoc


! sqlite3_column_props --
!    Convenience routine to set the properties of a column
! Arguments:
!    column        Column structure
!    name          Name of the column
!    type          Type of the column
!    length        Length if a string
! Side effects:
!    Fields in column filled
!

!> Convenience routine to set the properties of a column
!!
!! @note Side effects: Fields in column filled
subroutine sqlite3_column_props( column, name, type, length )
   implicit none
   !> Column structure
   type(SQLITE_COLUMN), intent(inout) :: column
   !> Name of the column
   character(len=*), intent(in)       :: name
   !> Type of the column
   integer, intent(in)                :: type
   !> Length if a string
   integer, intent(in), optional      :: length

   integer                            :: length_
   ! character(len=40)                  :: type_expr
   continue

   length_ = 20
   if ( present(length) ) then
      length_ = length
   endif

   column%name     = name
   column%type_set = type

   select case ( type )
   case (SQLITE_INT)
      column%type = 'INT'
   case (SQLITE_REAL)
      column%type = 'FLOAT'
   case (SQLITE_DOUBLE)
      column%type = 'DOUBLE'
   case (SQLITE_CHAR)
      write( column%type, '(a,i0,a)' ) 'CHAR(', length_, ')'
   case default
      column%type = 'UNKNOWN!'
   end select

   return
end subroutine sqlite3_column_props


! sqlite3_column_query --
!    Convenience routine to query a column or a function of that column
! Arguments:
!    column        Column structure
!    name          Name of the column
!    type          Type of the column
!    length        Length if a string (optional)
!    function      Name of the function to apply (if any)
! Side effects:
!    Fields in column filled
!

!> @brief Convenience routine to query a column or a function of that column
!!
!! @note Side effects: Fields in column filled
subroutine sqlite3_column_query( column, name, type, length, function )
   implicit none
   !> Column structure
   type(SQLITE_COLUMN), intent(inout)     :: column
   !> Name of the column
   character(len=*), intent(in)           :: name
   !> Type of the column
   integer, intent(in)                    :: type
   !> Length if a string (optional)
   integer, intent(in), optional          :: length
   !> Name of the function to apply (if any)
   character(len=*), intent(in), optional :: function
   continue

   column%function = ' '
   if ( present(function) ) then
      column%function = function
   endif
   if ( present(length) ) then
      call sqlite3_column_props( column, name, type, length )
   else
      call sqlite3_column_props( column, name, type )
   endif

   return
end subroutine sqlite3_column_query


! sqlite3_set_column_int    --
! sqlite3_set_column_real   --
! sqlite3_set_column_double --
! sqlite3_set_column_char   --
!    Convenience routines to set the value of a column
! Arguments:
!    column        Column structure
!    value         The value to be set
! Side effects:
!    Appropriate value field in column set
!

!> @brief Convenience routine to set the value of an integer column
!! @note Side effects: Appropriate value field in column set
subroutine sqlite3_set_column_int( column, value )
   implicit none
   !> Column structure
   type(SQLITE_COLUMN), intent(inout) :: column
   !> The value to be set
   integer, intent(in)                :: value

   continue

   column%int_value = value
   column%type_set  = SQLITE_INT

   return
end subroutine sqlite3_set_column_int

!> @brief Convenience routine to set the value of a single precision real column
!! @note Side effects: Appropriate value field in column set
subroutine sqlite3_set_column_real( column, value )
   implicit none
   !> Column structure
   type(SQLITE_COLUMN), intent(inout) :: column
   !> The value to be set
   real, intent(in)                   :: value

   continue

   column%double_value = value
   column%type_set  = SQLITE_DOUBLE

   return
end subroutine sqlite3_set_column_real

!> @brief Convenience routine to set the value of a double precision real column
!! @note Side effects: Appropriate value field in column set
subroutine sqlite3_set_column_double( column, value )
   implicit none
   !> Column structure
   type(SQLITE_COLUMN), intent(inout) :: column
   !> The value to be set
   real(kind=dp), intent(in)               :: value

   continue

   column%double_value = value
   column%type_set  = SQLITE_DOUBLE

   return
end subroutine sqlite3_set_column_double

!> @brief Convenience routine to set the value of a character column
!! @note Side effects: Appropriate value field in column set
subroutine sqlite3_set_column_char( column, value )
   implicit none
   !> Column structure
   type(SQLITE_COLUMN), intent(inout) :: column
   !> The value to be set
   character(len=*), intent(in)       :: value

   continue

   column%char_value = value
   column%type_set  = SQLITE_CHAR

   return
end subroutine sqlite3_set_column_char


! sqlite3_get_column_int    --
! sqlite3_get_column_real   --
! sqlite3_get_column_double --
! sqlite3_get_column_char   --
!    Convenience routines to get the value of a column
! Arguments:
!    column        Column structure
!    value         Value on return
! Side effects:
!    Value argument will be set
! Note:
!    No attempt is made to convert the value
!    to the requested value. You will have to
!    check this yourself
!

!> @brief Convenience routine to get the value of an integer column
subroutine sqlite3_get_column_int( column, value )
   implicit none
   !> Column structure
   type(SQLITE_COLUMN), intent(inout) :: column
   !> Value on return
   integer, intent(out)               :: value
   continue

   value = column%int_value

   return
end subroutine sqlite3_get_column_int

!> @brief Convenience routine to get the value of a single precision
!! real column
subroutine sqlite3_get_column_real( column, value )
   implicit none
   !> Column structure
   type(SQLITE_COLUMN), intent(inout) :: column
   !> Value on return
   real, intent(out)                  :: value
   continue

   value = column%double_value

   return
end subroutine sqlite3_get_column_real

!> @brief Convenience routine to get the value of a double precision
!! real column
subroutine sqlite3_get_column_double( column, value )
   implicit none
   !> Column structure
   type(SQLITE_COLUMN), intent(inout) :: column
   !> Value on return
   real(kind=dp), intent(out)              :: value
   continue

   value = column%double_value

   return
end subroutine sqlite3_get_column_double

!> @brief Convenience routine to get the value of a character (string)
!! column
subroutine sqlite3_get_column_char( column, value )
   implicit none
   !> Column structure
   type(SQLITE_COLUMN), intent(inout) :: column
   !> Value on return
   character(len=*), intent(out)      :: value
   continue

   value = column%char_value

   return
end subroutine sqlite3_get_column_char


! sqlite3_error --
!    Return the last error code
! Arguments:
!    db            Structure for the database
! Returns:
!    Last SQLite error code for this database
!

!> @brief Return the last error code
!!
!! @returns Last SQLite error code for this database (true or false)
logical function sqlite3_error( db )
   implicit none
   !> Structure for the database
   type(SQLITE_DATABASE) :: db
   continue

   ! sqlite3_error = (db%error /= 0)
   sqlite3_error = (db%error /= SQLITE_OK)                              &
      .and. (db%error /= SQLITE_ROW)                                    &
      .and. (db%error /= SQLITE_DONE)

   return
end function sqlite3_error


! sqlite3_errmsg --
!    Return the last error message
! Arguments:
!    db            Structure for the database
! Returns:
!    Last SQLite error message for this database
!

!> @brief Return the last error message
!!
!! @returns Last SQLite error message for this database (character)
character(len=80) function sqlite3_errmsg( db )
   implicit none
   !> Structure for the database
   type(SQLITE_DATABASE), intent(in) :: db
   continue

   sqlite3_errmsg = db%errmsg

   return
end function sqlite3_errmsg


! sqlite3_open --
!    Open a database file
! Arguments:
!    fname         Name of the file
!    db            Structure for the database
! Side effects:
!    The database file is opened and can be
!    used via the db argument
!

!> @brief Open a database file
!!
!! @note Side effects: The database file is opened and can be used via
!! the db argument
subroutine sqlite3_open( fname, db )
   implicit none
   !> Name of the file
   character(len=*), intent(in)      :: fname
   !> Structure for the database
   type(SQLITE_DATABASE), intent(inout) :: db

   character(len=len(fname)+1) :: fnamec

   interface
      integer function sqlite3_open_c( fnamec, handle, len_fnamec ) bind(C, name = 'sqlite3_open_c' )
         character(len=1), dimension(*) :: fnamec
         integer, dimension(*)          :: handle
         integer, value                 :: len_fnamec
      end function sqlite3_open_c
   end interface

   continue

   db%db_handle   = 0
   db%error       = 0
   db%errmsg       = ' '

   fnamec = fname
   call stringtoc( fnamec )

   db%error = sqlite3_open_c( fnamec, db%db_handle, len(fnamec) )

   return
end subroutine sqlite3_open


! sqlite3_close --
!    Close a database file
! Arguments:
!    db            Structure for the database
! Side effects:
!    The database file is closed and can no
!    longer be accessed
!

!> @brief Close a database file
!!
!! @note The database file is closed and can no longer be accessed
subroutine sqlite3_close( db )
   implicit none
   !> Structure for the database
   type(SQLITE_DATABASE) :: db

   interface
      integer function sqlite3_close_c( handle ) bind(C, name = 'sqlite3_close_c')
         integer, dimension(*) :: handle
      end function sqlite3_close_c
   end interface

   continue

   db%error = sqlite3_close_c( db%db_handle )
   db%db_handle   = 0

   return
end subroutine sqlite3_close


! sqlite3_do --
!    Run a single SQL command
! Arguments:
!    db            Structure for the database
!    command       Complete SQL command
! Side effects:
!    Whatever effects the command has. Note
!    that no output is reported back to the
!    caller (except for error codes and
!    messages if any)
!    longer be accessed
!

!> @brief Run a single SQL command
!! @note Side effects: Whatever effects the command has.
!! @note No output is reported back to the caller (except for error
!! codes and messages if any)
subroutine sqlite3_do( db, command )
   implicit none
   !> Structure for the database
   type(SQLITE_DATABASE) :: db
   !> Complete SQL command
   character(len=*)      :: command

   interface
      integer function sqlite3_do_c( handle, command, errmsg, len_command, len_errmsg ) bind(C, name = 'sqlite3_do_c')
         integer, dimension(*)          :: handle
         character(len=1), dimension(*) :: command
         character(len=1), dimension(*) :: errmsg
         integer, value                 :: len_command
         integer, value                 :: len_errmsg
      end function sqlite3_do_c
   end interface

   character(len=len(command)+1) :: commandc
   ! integer                       :: k

   continue

   commandc = command
   call stringtoc( commandc )

   db%errmsg = ' '
   db%error  = sqlite3_do_c( db%db_handle, commandc, db%errmsg, len(commandc), len(db%errmsg) )

   return
end subroutine sqlite3_do


! sqlite3_begin --
!    Start a transaction on the given database
! Arguments:
!    db            Structure for the database
! Note:
!    Should be accompanied by a call to either
!    sqlite3_commit or sqlite3_rollback
!

!> @brief Start a transaction on the given database
!!
!! @note Should be accompanied by a call to either
!! sqlite3_commit or sqlite3_rollback
subroutine sqlite3_begin( db )
   implicit none
   !> Structure for the database
   type(SQLITE_DATABASE) :: db
   continue

   call sqlite3_do( db, "BEGIN TRANSACTION" )

   return
end subroutine sqlite3_begin


! sqlite3_commit --
!    Commits a transaction on the given database
! Arguments:
!    db            Structure for the database
! Note:
!    Accompanies sqlite3_begin
!

!> @brief Commits a transaction on the given database
!! @note Accompanies sqlite3_begin
subroutine sqlite3_commit( db )
   implicit none
   !> Structure for the database
   type(SQLITE_DATABASE) :: db
   continue

   call sqlite3_do( db, "COMMIT TRANSACTION" )

   return
end subroutine sqlite3_commit


! sqlite3_rollback --
!    Rolls back any changes to the database since the last commit
! Arguments:
!    db            Structure for the database
! Note:
!    Accompanies sqlite3_begin
!

!> @brief Rolls back any changes to the database since the last commit
!! @note Accompanies sqlite3_begin
subroutine sqlite3_rollback( db )
   implicit none
   !> Structure for the database
   type(SQLITE_DATABASE) :: db
   continue

   call sqlite3_do( db, "ROLLBACK" )

   return
end subroutine sqlite3_rollback


! sqlite3_delete_table --
!    Delete a table
! Arguments:
!    db            Structure for the database
!    tablename     Name of the table to be deleted
! Note:
!    The table can not be recovered, unless this
!    is part of a transaction
!

!> @brief Delete a table
!! @note The table can not be recovered, unless this
!! is part of a transaction
subroutine sqlite3_delete_table( db, tablename )
   implicit none
   !> Structure for the database
   type(SQLITE_DATABASE), intent(inout) :: db
   !> Name of the table to be deleted
   character(len=*), intent(in)      :: tablename

   character(len=20+len(tablename)) :: command

   continue

   write( command, "(2A)" ) "DELETE TABLE ", tablename
   call sqlite3_do( db, command )

   return
end subroutine sqlite3_delete_table


! sqlite3_create_table --
!    Create a new table
! Arguments:
!    db            Structure for the database
!    tablename     Name of the table
!    columns       Properties of the columns
!    primary       Name of the primary key (if any)
! Side effects:
!    The new table is created
!

!> @brief Create a new table
!!
!! @note Side effects: The new table is created
subroutine sqlite3_create_table( db, tablename, columns, primary )
   implicit none
   !> Structure for the database
   type(SQLITE_DATABASE), intent(inout)           :: db
   !> Name of the table
   character(len=*), intent(in)                   :: tablename
   !> Properties of the columns
   type(SQLITE_COLUMN), dimension(:)  :: columns
   !> Name of the primary key (if any)
   character(len=*), optional         :: primary

   character(len=20+80*size(columns)) :: command
   character(len=40)                  :: primary_
   character(len=40)                  :: format
   integer                            :: i
   integer                            :: ncols

   continue

   primary_ = ' '
   if ( present(primary) ) then
      primary_ = primary
   endif

   ncols = size(columns)
   write( format, '(a,i0,a)' ) '(', 4 + 4 * ncols, 'a)'
   write( command, format ) 'create table ', tablename, ' (', &
      ( trim(columns(i)%name), ' ', trim(typename(columns(i), primary_)), ', ', &
           i = 1,ncols-1 ), &
      trim(columns(ncols)%name), ' ', trim(typename(columns(ncols),primary_)), ')'

   call sqlite3_do( db, command )

   return
end subroutine sqlite3_create_table


! sqlite3_prepare_select --
!    Prepare a selection of data from the database
! Arguments:
!    db            Structure for the database
!    tablename     Name of the table
!    columns       Columns to be returned
!    stmt          Prepared statement (returned)
!    extra_clause  Extra clause for SELECT statement (appended)
! Side effects:
!    A new selection is prepared
!

!> @brief Prepare a selection of data from the database
!! @note Side effects: A new selection is prepared
subroutine sqlite3_prepare_select( db, tablename, columns, stmt, extra_clause )
   implicit none
   !> Structure for the database
   type(SQLITE_DATABASE), intent(inout)        :: db
   !> Name of the table
   character(len=*), intent(in)                :: tablename
   !> List of columns (returned)
   type(SQLITE_COLUMN), dimension(:), pointer, intent(out)  :: columns     ! On return: actual columns!
   !> Prepared statement (returned)
   type(SQLITE_STATEMENT), intent(out)         :: stmt
   !> Extra clause for SELECT statement (appended)
   character(len=*), optional, intent(in)      :: extra_clause

   character(len=20+80*size(columns))          :: command
   character(len=40)                           :: format
   integer                                     :: nocols
   integer                                     :: i

   continue

   !
   ! Prepare the select statement for this table
   !
   ! TODO: expand the syntax!!
   !
   nocols = size(columns)
   write( format, '(a,i0,a)' ) '(', 4 + 2 * nocols, 'a)'
   write( command, format ) 'select ', &
      (trim(column_func(columns(i))), ',', i = 1,nocols-1), &
       trim(column_func(columns(nocols))), &
      ' from ', trim(tablename)

   !
   ! Hm, appending a string of arbitrary length is tricky ...
   !
   if ( present(extra_clause) ) then
      command = trim(command) // ' ' // extra_clause
   endif

   call stringtoc( command )
   call sqlite3_prepare( db, command, stmt, columns )

   return
end subroutine sqlite3_prepare_select

! sqlite3_insert --
!    Insert a row into the given table
! Arguments:
!    db            Structure for the database
!    tablename     Name of the table
!    columns       Columns whose value is to be inserted
! Side effects:
!    A new row is written to the database
!

!> @brief Insert a row into the given table
!! @note Side effects: A new row is written to the database
subroutine sqlite3_insert( db, tablename, columns )
   !> Structure for the database
   type(SQLITE_DATABASE)                       :: db
   !> Name of the table
   character(len=*)                            :: tablename
   !> Columns whose value is to be inserted
   type(SQLITE_COLUMN), dimension(:), target   :: columns

   character(len=20+80*size(columns))          :: command
   character(len=40)                           :: format

   type(SQLITE_COLUMN), dimension(:), pointer  :: prepared_columns
   type(SQLITE_STATEMENT)                      :: stmt
   integer                                     :: i
   integer                                     :: rc

   interface
      subroutine sqlite3_errmsg_c( handle, errmsg, len_errmsg ) bind(C, name = 'sqlite3_errmsg_c')
         integer, dimension(*)          :: handle
         character(len=1), dimension(*) :: errmsg
         integer, value                 :: len_errmsg
      end subroutine sqlite3_errmsg_c
   end interface

   interface
      integer function sqlite3_bind_int_c( handle, colidx, value ) bind(C, name = 'sqlite3_bind_int_c')
         integer, dimension(*) :: handle
         integer               :: colidx
         integer               :: value
      end function sqlite3_bind_int_c
   end interface

   interface
      integer function sqlite3_bind_double_c( handle, colidx, value ) bind(C, name = 'sqlite3_bind_double_c')
         use sqlite_types
         integer, dimension(*) :: handle
         integer               :: colidx
         real(kind=dp)         :: value
      end function sqlite3_bind_double_c
   end interface

   interface
      integer function sqlite3_bind_text_c( handle, colidx, value, len_value ) bind(C, name = 'sqlite3_bind_text_c')
         integer, dimension(*)          :: handle
         integer                        :: colidx
         character(len=1), dimension(*) :: value
         integer, value                 :: len_value
      end function sqlite3_bind_text_c
   end interface

   continue

   !
   ! Prepare the insert statement for this table
   !
   write( format, '(a,i0,a)' ) '(', 4 + 2 * size(columns), 'a)'
   write( command, format ) 'insert into ', trim(tablename), ' values(', &
      ('?,', i = 1,size(columns)-1), '?)'

   call stringtoc( command )
   prepared_columns => columns
   call sqlite3_prepare( db, command, stmt, prepared_columns )

   !
   ! Bind the values
   !
   do i = 1,size(columns)
      select case (columns(i)%type_set)
      case (SQLITE_INT)
         rc = sqlite3_bind_int_c( stmt%stmt_handle, i, columns(i)%int_value )
      case (SQLITE_DOUBLE)
         rc = sqlite3_bind_double_c( stmt%stmt_handle, i, columns(i)%double_value )
      case (SQLITE_CHAR)
         rc = sqlite3_bind_text_c( stmt%stmt_handle, i, trim(columns(i)%char_value), len_trim(columns(i)%char_value) )
      end select
      if ( rc .ne. 0 ) then
         db%error = rc
         call sqlite3_errmsg_c( db%db_handle, db%errmsg, len(db%errmsg) )
         call stringtof( db%errmsg )
      endif
   enddo

   !
   ! Actually perform the insert command
   !
   call sqlite3_step( stmt, rc )
   call sqlite3_finalize( stmt )

   return
end subroutine sqlite3_insert


! sqlite3_next_row --
!    Gets the next row of data from a selection
! Arguments:
!    stmt          Prepared statement
!    columns       Columns to be returned
!    finished      Indicates there are no more data
!

!> @brief Gets the next row of data from a selection
subroutine sqlite3_next_row( stmt, columns, finished )
   implicit none
   !> Prepared statement
   type(SQLITE_STATEMENT), intent(inout)            :: stmt
   !> Columns to be returned
   type(SQLITE_COLUMN), dimension(:), intent(inout) :: columns
   !> Indicates there are no more data
   logical, intent(inout)                        :: finished

   interface
      integer function sqlite3_column_int_c( handle, colidx, value ) bind(C, name = 'sqlite3_column_int_c')
         integer, dimension(*) :: handle
         integer               :: colidx
         integer               :: value
      end function sqlite3_column_int_c
   end interface

   interface
      integer function sqlite3_column_double_c( handle, colidx, value ) bind(C, name = 'sqlite3_column_double_c')
         use sqlite_types
         integer, dimension(*) :: handle
         integer               :: colidx
         real(kind=dp)         :: value
      end function sqlite3_column_double_c
   end interface

   interface
      integer function sqlite3_column_text_c( handle, colidx, value, len_value ) bind(C, name = 'sqlite3_column_text_c')
         integer, dimension(*)          :: handle
         integer                        :: colidx
         character(len=1), dimension(*) :: value
         integer, value                 :: len_value
      end function sqlite3_column_text_c
   end interface

   integer                           :: rc
   integer                           :: i

   continue

   call sqlite3_step( stmt, rc )

   if ( rc .eq. SQLITE_ROW ) then
      finished = .false.

      !
      ! Get the values
      !
      ! TODO: check validity of "type_set"
      !
      do i = 1,size(columns)
         select case (columns(i)%type_set)
         case (SQLITE_INT)
            rc = sqlite3_column_int_c( stmt%stmt_handle, i-1, columns(i)%int_value )
         case (SQLITE_REAL,SQLITE_DOUBLE)
            rc = sqlite3_column_double_c( stmt%stmt_handle, i-1, columns(i)%double_value )
         case (SQLITE_CHAR)
            rc = sqlite3_column_text_c( stmt%stmt_handle, i-1, columns(i)%char_value, len(columns(i)%char_value) )
            call stringtof( columns(i)%char_value )
         end select
        ! if ( rc .ne. 0 ) then
        !    db%error = rc
        !    call sqlite3_errmsg_c( db%db_handle, db%errmsg )
        !    call stringtof( db%errmsg )
        ! endif
      enddo
   else
      finished = .true.
   endif

   return
end subroutine sqlite3_next_row


! sqlite3_query_table --
!    Retrieve the column names and types from a table
! Arguments:
!    db            Structure for the database
!    tablename     Name of the table
!    columns       Columns (allocated array)
! Side effects:
!    The columns array is allocated and filled
! Note:
!    On entry the columns argument must not be
!    associated. On exit, it will point to a
!    freshly allocated array of column names/types
!

!> @brief Retrieve the column names and types from a table
!!
!! @note On entry the columns argument must not be associated. On exit,
!! it will point to a freshly allocated array of column names/types
subroutine sqlite3_query_table( db, tablename, columns )
   implicit none
   !> Structure for the database
   type(SQLITE_DATABASE)                       :: db
   !> Name of the table
   character(len=*)                            :: tablename
   !> Columns (allocated array)
   type(SQLITE_COLUMN), dimension(:), pointer  :: columns     ! On return: actual columns!

   type(SQLITE_STATEMENT)                      :: stmt
   character(len=20+len(tablename))            :: command

   continue

   write( command, '(2a)' ) 'select * from ',tablename

   !
   ! Note, we must free the columns, but we can not be sure
   ! they are no longer used. So simply disassociate.
   if ( associated(columns) ) then
      nullify( columns )
   endif
   call sqlite3_prepare( db, command, stmt, columns )
   call sqlite3_finalize( stmt )

   return
end subroutine sqlite3_query_table


! sqlite3_finalize --
!    Finalize the prepared SQL statement
! Arguments:
!    stmt          Handle to the prepared statement
!

!> @brief Finalize the prepared SQL statement
subroutine sqlite3_finalize( stmt )
   implicit none
   !> Handle to the prepared statement
   type(SQLITE_STATEMENT)                      :: stmt

   interface
      subroutine sqlite3_finalize_c( stmt ) bind(C, name = 'sqlite3_finalize_c')
         integer, dimension(*) :: stmt
      end subroutine sqlite3_finalize_c
   end interface

   continue

   call sqlite3_finalize_c( stmt%stmt_handle )

   return
end subroutine sqlite3_finalize


! sqlite3_reset --
!    Reset the prepared SQL statement so that it can
!    be used again
! Arguments:
!    stmt          Handle to the prepared statement
!

!> @brief Reset the prepared SQL statement so that it can be used again
subroutine sqlite3_reset( stmt )
   implicit none
   !> Handle to the prepared statement
   type(SQLITE_STATEMENT)                      :: stmt

   interface
      subroutine sqlite3_reset_c( stmt ) bind(C, name = 'sqlite3_reset_c')
         integer, dimension(*) :: stmt
      end subroutine sqlite3_reset_c
   end interface

   continue

   call sqlite3_reset_c( stmt%stmt_handle )

   return
end subroutine sqlite3_reset


! sqlite3_step --
!    Run the prepared SQL statement
! Arguments:
!    stmt          Handle to the prepared statement
!    completion    Return code, indicating if the command is complete or
!                  not (SQLITE_DONE, SQLITE_MISUSE or SQLITE_ERROR)
!

!> @brief Run the prepared SQL statement
subroutine sqlite3_step( stmt, completion )
   implicit none
   !> Handle to the prepared statement
   type(SQLITE_STATEMENT), intent(inout)       :: stmt
   !> Return code, indicating if the command is complete or
   !! not (SQLITE_DONE, SQLITE_MISUSE or SQLITE_ERROR)
   integer, intent(out)                        :: completion

   interface
      subroutine sqlite3_step_c( stmt, completion ) bind(C, name = 'sqlite3_step_c')
         integer, dimension(*) :: stmt
         integer               :: completion
      end subroutine sqlite3_step_c
   end interface

   continue

   call sqlite3_step_c( stmt%stmt_handle, completion )

   return
end subroutine sqlite3_step


! sqlite3_prepare --
!    Prepare the SQL statement for actual use
! Arguments:
!    stmt          Handle to the prepared statement
!

!> @brief Prepare the SQL statement for actual use
subroutine sqlite3_prepare( db, command, stmt, columns )
   implicit none
   !> Handle to the database
   type(SQLITE_DATABASE), intent(inout)        :: db
   !> Command to send to database
   character(len=*), intent(in)                :: command
   !> Handle to the prepared statement
   type(SQLITE_STATEMENT), intent(out)         :: stmt
   !> Pointers to database columns
   type(SQLITE_COLUMN), dimension(:), pointer  :: columns     ! On return: actual columns!

   interface
      subroutine sqlite3_errmsg_c( handle, errmsg, len_errmsg ) bind(C, name = 'sqlite3_errmsg_c')
         integer, dimension(*)          :: handle
         character(len=1), dimension(*) :: errmsg
         integer, value                 :: len_errmsg
      end subroutine sqlite3_errmsg_c
   end interface

   interface
      integer function sqlite3_prepare_c( db, command, stmt, len_command ) bind(C, name = 'sqlite3_prepare_c')
         integer, dimension(*)          :: db
         character(len=1), dimension(*) :: command
         integer, dimension(*)          :: stmt
         integer, value                 :: len_command
      end function sqlite3_prepare_c
   end interface

   interface
      subroutine sqlite3_column_count_c( handle, count ) bind(C, name = 'sqlite3_column_count_c')
         integer, dimension(*) :: handle
         integer               :: count
      end subroutine sqlite3_column_count_c
   end interface

   interface
      subroutine sqlite3_column_name_type_c( handle, colidx, name, type, &
            len_name, len_type ) bind(C, name = 'sqlite3_column_name_type_c')
         integer, dimension(*)          :: handle
         integer                        :: colidx
         character(len=1), dimension(*) :: name
         character(len=1), dimension(*) :: type
         integer, value                 :: len_name
         integer, value                 :: len_type
      end subroutine sqlite3_column_name_type_c
   end interface

   integer                                     :: count
   integer                                     :: i
   character(len=len(command)+1)               :: commandc

   continue

   commandc = command
   call stringtoc( commandc )
   db%error = sqlite3_prepare_c( db%db_handle, commandc, stmt%stmt_handle, len(commandc) )

   if ( db%error .eq. 0 ) then
      if ( associated(columns) ) return ! Assumption: they are already known

      call sqlite3_column_count_c( stmt%stmt_handle, count )

      allocate( columns(1:count) )

      do i = 1,count
         call sqlite3_column_name_type_c( stmt%stmt_handle, i-1, &
            columns(i)%name, columns(i)%type, len(columns(i)%name), len(columns(i)%type) )
         call stringtof( columns(i)%name )
         call stringtof( columns(i)%type )

         select case (columns(i)%type(1:4) )
         case( 'INT ', 'INTE' )
            columns(i)%type_set = SQLITE_INT
         case( 'FLOA', 'DOUB' )
            columns(i)%type_set = SQLITE_DOUBLE
         case( 'CHAR', 'VARC' )
            columns(i)%type_set = SQLITE_CHAR
         end select

      enddo
   else
      call sqlite3_errmsg_c( db%db_handle, db%errmsg, len(db%errmsg) )
   endif

   return
end subroutine sqlite3_prepare


! sqlite3_get_table --
!    Call sqlite3_exec() and return the result in an
!    array of strings
! Arguments:
!    db            Handle to the database
!    command       SQL comman to be executed
!    result        Two-dimensional array of strings (pointer)
!    errmsg        Error message (if any)
! Note:
!    The result array is _nullified_ first, then allocated
!    to hold the resulting table (within the limits of the
!    character strings). It is up to the user to deallocate
!    this array when done.
! Further note:
!    Because we have to split the process into two parts,
!    to allocate an array that is large enough to hold all
!    strings, use is made of a static variable. As a consequence
!    this routine is _not_ thread-safe.
!

!> @brief Call sqlite3_exec() and return the result in an array of
!! strings
!!
!! @note The result array is _nullified_ first, then allocated
!! to hold the resulting table (within the limits of the
!! character strings). It is up to the user to deallocate
!! this array when done.
!!
!! @note Because we have to split the process into two parts,
!! to allocate an array that is large enough to hold all
!! strings, use is made of a static variable. As a consequence
!! this routine is _not_ thread-safe.
subroutine sqlite3_get_table( db, command, result, errmsg )
   implicit none
   !> Handle to the database
   type(SQLITE_DATABASE), intent(inout)                     :: db
   !> SQL command to be executed
   character(len=*), intent(in)                             :: command
   !> Two-dimensional array of strings (pointer)
   character(len=*), pointer, dimension(:,:), intent(inout) :: result
   !> Error message (if any)
   character(len=*), intent(out)                            :: errmsg

   character(len=len(command)+1)               :: commandc
   integer                                     :: ncol
   integer                                     :: nrow

   interface
      integer function sqlite3_get_table_1_c( handle, commandc, ncol, &
            nrow, errmsg, len_commandc, len_errmsg ) bind(C, name = 'sqlite3_get_table_1_c')
         integer, dimension(*)          :: handle
         character(len=1), dimension(*) :: commandc
         integer                        :: ncol
         integer                        :: nrow
         character(len=1), dimension(*) :: errmsg
         integer, value                 :: len_commandc
         integer, value                 :: len_errmsg
      end function sqlite3_get_table_1_c
   end interface

   interface
      subroutine sqlite3_get_table_2_c( ncol, nrow, result, len_result ) bind(C, name = 'sqlite3_get_table_2_c' )
         integer                       :: ncol
         integer                       :: nrow
         character(len=1),dimension(*) :: result
         integer, value                :: len_result
      end subroutine sqlite3_get_table_2_c
   end interface

   continue

   commandc = command
   call stringtoc( commandc )

   db%error  = sqlite3_get_table_1_c( db%db_handle, commandc, ncol, nrow, db%errmsg, &
                   len(commandc), len(db%errmsg) )

   nullify( result )
   if ( db%error == 0 ) then
       allocate( result(ncol,nrow+1) )
       call sqlite3_get_table_2_c( ncol, nrow, result, len(result) )
   endif

   errmsg = db%errmsg

   return
end subroutine sqlite3_get_table

end module sqlite
