!> @file csv_to_db.f90
!! @author Arjen Markus
!! @copyright See LICENSE; BSD 3-Clause.

! csv_to_db.f90 --
!    Program to read a simple CSV file and put it into a
!    SQLite database, just to demonstrate how the Fortran
!    interface works.
!
!    To keep it simple:
!    - The first line contains the names of the four columns
!    - All lines after that contain the name of the station
!      the date and the two values.
!

!> @brief Program to read a simple CSV file and put it into a
!! SQLite database, just to demonstrate how the Fortran
!! interface works.
!!
!! To keep it simple:
!!  - The first line contains the names of the four columns
!!  - All lines after that contain the name of the station,
!!    the date, and the two values.
program csv_to_db
   use sqlite

   implicit none

   character(len=*), parameter                :: infile = 'somedata.csv'
   character(len=*), parameter                :: outfile = 'somedata.db'

   type(SQLITE_DATABASE)                      :: db
   type(SQLITE_STATEMENT)                     :: stmt
   type(SQLITE_COLUMN), dimension(:), pointer :: column

   integer                                    :: lun
   integer                                    :: j
   integer                                    :: ierr
   character(len=40), dimension(4)            :: name
   real                                       :: salin
   real                                       :: temp
   character(len=40)                          :: station
   character(len=40)                          :: date
   logical                                    :: finished

   character(len=40), pointer, dimension(:,:) :: result
   character(len=80)                          :: errmsg

   logical                                    :: stale_file
   integer                                    :: sfh

   continue

   !
   ! Read the CSV file and feed the data into the database
   !
   open( newunit = lun, file = infile )
   read( unit = lun, fmt = * ) name

   inquire(file = outfile, exist = stale_file)
   if (stale_file) then
      open(newunit = sfh, file = outfile, iostat = ierr, status='OLD')
      if (ierr == 0) then
         close(unit = sfh, status = 'DELETE')
      end if
   end if

   call sqlite3_open( outfile, db )

   allocate( column(4) )
   call sqlite3_column_props( column(1), name(1), SQLITE_CHAR, 10 )
   call sqlite3_column_props( column(2), name(2), SQLITE_CHAR, 10 )
   call sqlite3_column_props( column(3), name(3), SQLITE_REAL )
   call sqlite3_column_props( column(4), name(4), SQLITE_REAL )
   call sqlite3_create_table( db, 'measurements', column )

   !
   ! Insert the values into the table. For better performance,
   ! make sure (via begin/commit) that the changes are committed
   ! only once.
   !
   call sqlite3_begin( db )
   do
      read( unit = lun, fmt = *, iostat = ierr ) station, date, salin, temp

      if ( ierr .ne. 0 ) exit

      call sqlite3_set_column( column(1), station )
      call sqlite3_set_column( column(2), date    )
      call sqlite3_set_column( column(3), salin   )
      call sqlite3_set_column( column(4), temp    )
      call sqlite3_insert( db, 'measurements', column )

   enddo

   close( unit = lun )

   call sqlite3_commit( db )

   !
   ! We want a simple report, the mean of salinity and temperature
   ! sorted by the station
   !
   deallocate( column )
   allocate( column(3) )
   call sqlite3_column_query( column(1), 'station', SQLITE_CHAR )
   call sqlite3_column_query( column(2), name(3), SQLITE_REAL, function='avg' )
   call sqlite3_column_query( column(3), name(4), SQLITE_REAL, function='avg' )
   call sqlite3_prepare_select( db, 'measurements', column, stmt, &
      'group by station order by station' )

   write( *, '(3a20)' ) 'Station', 'Mean salinity', 'Mean temperature'
   do
      call sqlite3_next_row( stmt, column, finished )

      if ( finished ) exit

      call sqlite3_get_column( column(1), station )
      call sqlite3_get_column( column(2), salin   )
      call sqlite3_get_column( column(3), temp    )

      write( *, '(a20,2f20.3)' ) station, salin, temp
   end do
   ! Destroy statement handle after use
   call sqlite3_finalize( stmt )

   !
   ! Get the entire table
   !
   call sqlite3_get_table( db, "select * from measurements", result, errmsg )

   if ( associated(result) ) then
      write(*,*) 'Number of columns: ', size(result,1)
      write(*,*) 'Number of rows:    ', size(result,2)
      do j = 1,size(result,2)
         write(*,'(10a20)') result(:,j)
      enddo
      deallocate( result )
   else
      write(*,*) 'Error: ', trim(errmsg)
   endif

   call sqlite3_close( db )
   if ( sqlite3_error(db) ) then
       write(*,'(A, I4, A)') 'Error closing the database: (', db%error, &
         ') "' // trim(sqlite3_errmsg(db)) // '"'
   endif
end program csv_to_db
